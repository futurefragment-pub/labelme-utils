#!/usr/bin/env bash
python setup.py sdist
python3 -m twine upload dist/*
#python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*